# Ranking

## Description

Ce projet fait office de rendu pour le dernier TP d'Indexation Web. Le but de projet est de concevoir un système qui prend en charge la requête d'un utilisateur. Le système doit ensuite tokeniser et transformer cette requête afin de pouvoir l'exploiter dans le but de filtrer les documents qui ont les tokens de la requête puis d'ordonner ces documents en les renvoyants dans un fichier 'results.json'. 

## Installation

Pour installer le projet :

```
git clone https://gitlab.com/BadrKe/ranking.git
cd ranking
```

## Lancer le projet

Pour lancer le projet :

```
python3 main.py
```

Suite à ça, l'interface de votre terminal vous demandera de tapez votre requête.
Ensuite, l'interface vous demandera si vous voulez que les documents trouvés contiennent exactement tout les mots de votre requêtes. Pour que cela soit le cas, écrivez simplement "y" sur votre terminal. Si vous écrivez une quelconque autre lettre ou appuyez directement sur entrée, alors l'application partira du principe que vous ne voulez pas forcément avoir tout les mots de votre requête dans la recherche.

## Lancer les tests

```
python3 -m unittest test/test.py
```

## Auteur

Badr KEBRIT