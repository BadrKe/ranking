from ranking import Ranking

if __name__ == '__main__':
    request = input('Veuillez tapez votre recherche :')
    awnser = input(
        'Voulez-vous prendre en compte uniquement les documents qui ont tout les tokens de votre requête ? (Y/N) ')
    if awnser.lower() == 'y':
        request_all = True
    else:
        request_all = False

    rank = Ranking('index.json', 'documents.json', request, request_all)
    ranked_id = rank.ranking()
    rank.to_json(ranked_id)