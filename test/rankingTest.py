import json
import unittest
from ranking import Ranking


class RankingTest(unittest.TestCase):

    def test_tokenize(self):
        test_request = 'Ceci est un TEST de requête'
        ranking = Ranking('index.json', 'documents.json', test_request, True)

        tokenized_request = ranking.tokenize()
        self.assertEqual(tokenized_request, [
                         'ceci', 'est', 'un', 'test', 'de', 'requête'])

    def test_filter(self):
        test_request = 'bienvenue imagerie'
        ranking = Ranking('index.json', 'documents.json', test_request, True)

        filtered_index = ranking.filter()

        self.assertEqual(filtered_index, {'bienvenue': {'21': {'positions': [0], 'count': 1}, '51': {'positions': [
                         0], 'count': 1}, '78': {'positions': [0], 'count': 1}}, 'imagerie': {'21': {'positions': [10], 'count': 1}}})
    
