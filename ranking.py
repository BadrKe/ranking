import json


class Ranking():

    def __init__(self, index_path, document_path, request, request_all):
        with open(index_path) as file:
            self.index = json.load(file)
        with open(document_path) as file:
            self.document = json.load(file)
        self.request = request
        self.request_all = request_all

    def tokenize(self):
        '''
        Cette méthode permet simplement de tokeniser la requête de l'utilisateur.
        Elle transforme la chaîne de caractère en minuscule et la coupe en fonction des espaces.
        '''
        return self.request.lower().split(' ')

    def filter(self):
        '''
        Cette méthode permet de filtrer naïvement l'index en fonction des tokens de la requête.
        Elle crée un nouvel index filtré, qui va garder uniquement les documents qui contienennt au moins un token de la requête.
        '''

        filtered_index = {}

        tokenized_request = self.tokenize()
        for token in tokenized_request:
            if token in self.index:
                filtered_index[token] = self.index[token]
        return filtered_index

    def ranking(self):
        '''
        Cette méthode permet d'ordonner les documents. Elle va permettre aussi d'affiner le filtrage.
        Si self.request_all = True , alors la méthode va faire en sorte de garder uniquement les documents avec tout les tokens dedans.
        '''

        filtered_index = self.filter()
        ranked_id = {}

        if self.request_all:

            website_id = []
            for token in filtered_index:
                # On crée une liste contenant des listes d'ID des website de chaque token
                website_id.append([id for id in filtered_index[token]])

            # La liste de liste webstide_id va servir à faire des intersections entre les différentes listes d'id de website par token
            sorted_id_list = website_id[0]
            for list_id in website_id:
                sorted_id_list = list(set(sorted_id_list) & set(list_id))

            for token in filtered_index:
                for website_id in sorted_id_list:
                    if website_id in ranked_id:
                        ranked_id[website_id] += filtered_index[token][website_id]['count']
                    else:
                        ranked_id[website_id] = filtered_index[token][website_id]['count']

        else:
            for token in filtered_index:
                for website_id in filtered_index[token]:
                    if website_id in ranked_id:
                        ranked_id[website_id] += filtered_index[token][website_id]['count']
                    else:
                        ranked_id[website_id] = filtered_index[token][website_id]['count']

        return ranked_id

    def to_json(self, ranked_id):
        '''
        Cette méthode permet simplement d'écrire dans un json les résultats du ranking.
        '''
        results = []
        for id in ranked_id:
            for website in self.document:
                if id == str(website['id']):
                    results.append(
                        {'Titre': website['title'], 'URL': website['url']})

        with open('results.json', 'w', encoding='utf-8') as file:
            json.dump(results, file)
